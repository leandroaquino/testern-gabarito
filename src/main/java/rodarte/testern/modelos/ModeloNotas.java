package rodarte.testern.modelos;

public class ModeloNotas {

	private long id;
	private int periodo;
	private double nota;
	
	public ModeloNotas(int periodo, double nota) {
		
		this.periodo = periodo;
		this.nota = nota;
		
	}

	public ModeloNotas(long id, int periodo, double nota) {
		this.id = id;
		this.periodo = periodo;
		this.nota = nota;
	}

	public ModeloNotas() {
	}

	public long getId() {
		return this.id;
	}

	public int getPeriodo() {
		return this.periodo;
	}

	public double getNota() {
		return this.nota;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setPeriodo(int periodo) {
		this.periodo = periodo;
	}

	public void setNota(double nota) {
		this.nota = nota;
	}
}